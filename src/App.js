import './App.css';
import { useEffect, useState} from "react";
import RenderImage from "./components/RenderImage";
import useFetchImages from "./services/fetchImages";
function App() {

    const [search, setSearch] = useState("cat")
    const url = `https://www.flickr.com/services/feeds/photos_public.gne?tags=${search}&format=json&tagmode=any`

    const [fetch, setFetch] = useState(false)
    const {fetchImages, images,setImages,error} = useFetchImages();
    let timer = null;


    useEffect(() => {
        const abortController = new AbortController();

        if (fetch) {
            if (search) {
                fetchImages(url);
            }
        }else{
            setImages([])
        }
        return () => abortController.abort();
    }, [fetch]);

    const RenderImagers = () => {
        return( images.map((image,index) => RenderImage({image,index})))
    }



    const searchFlickrFeed = (e) => {
        setSearch(e.target.value)
        clearTimeout(timer)

       timer = setTimeout(() => {
            setFetch(true)
           clearTimeout(timer)

        }, 500)
        if(fetch === true){
            setFetch(false);
        }
    }

  return (
    <div className="App">
      <header className="App-header">
          <img className="icon" src="flickr_logo.jpeg" alt=""/>
          <input className='search-field' type="text" onInput={searchFlickrFeed}/>
          <div className="image-container">
              <div>{error}</div>
          <RenderImagers images={images} />
          </div>
      </header>
    </div>
  );
}

export default App;
