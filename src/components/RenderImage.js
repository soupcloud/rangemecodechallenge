
function RenderImage({image,index}) {
        return(
            <div className="image"  key={index}>
                <img data-testid="image"   className="image" src={image.media.m}  alt=""/>
                <h1 data-testid="title" >{image.title}</h1>
                <div data-testid="author" className="image-info">Author: {image.author}</div>
                <div data-testid="date"  className="image-info">Date Taken: {image.date_taken} </div>
                <div data-testid="tags"  className="image-info">Tags:{image.tags}</div>
                <div data-testid="link"  className="image-info">Link: <a href={image.link}>Open</a> </div>
            </div>
        )
}
export default RenderImage
