import fetchJsonp from "fetch-jsonp";
import {useState} from "react";

export default function useFetchImages() {
    const [images, setImages] = useState([])
    const [error, setError] = useState()


    const  fetchImages = (url) => {
        fetchJsonp(url, {
            jsonpCallback: 'jsoncallback',
            timeout: 3000
        })
            .then((response) => response.json())
            .then((json) => setImages(json.items))
            .catch((error) => {
                setError("Error occurred while fetching Photos")
            });
    }

    return {fetchImages, images, setImages,error}
}

