// import { render, screen } from '@testing-library/react';
import useFetchImages from "../services/fetchImages";
import { renderHook, act } from '@testing-library/react-hooks';
import fetchJsonp from 'fetch-jsonp';
import TestImages from "./test_data";
//

const unmockedFetch = global.fetchJsonp

beforeAll(() => {
    global.fetchJsonp = () =>
        console.log('asdf')
        Promise.resolve({
            json: () => Promise.resolve(TestImages),
        })
})

afterAll(() => {
    global.fetchJsonp = unmockedFetch
})

test('fetch images', () => {
    const {result} = renderHook(() => useFetchImages());
    const url = `https://www.flickr.com/services/feeds/photos_public.gne?tags=cats&format=json&tagmode=any`
    act(() => {
            result.current.fetchImages(url)
        }
    );

    expect(result.current.images).toEqual([])
});
