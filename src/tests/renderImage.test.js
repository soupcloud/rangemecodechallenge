import { render, screen } from '@testing-library/react';
import RenderImage from "../components/RenderImage";
import TestImages from "./test_data";

test('renders requested attributes', () => {
    render(<RenderImage image={TestImages.items[0]} index={0} />   );
    const author = screen.getByTestId('author');
    expect(author).toBeInTheDocument()
    const date = screen.getByTestId('date');
    expect(date).toBeInTheDocument()
    const image = screen.getByTestId('image');
    expect(image).toBeInTheDocument()
    const title = screen.getByTestId('title');
    expect(title).toBeInTheDocument()
    const tags = screen.getByTestId('tags');
    expect(tags).toBeInTheDocument()
    const link = screen.getByTestId('link');
    expect(link).toBeInTheDocument()

});
